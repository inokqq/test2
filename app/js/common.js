var h = document.getElementById('new-main').offsetHeight;
var dh = document.getElementById('discheight').offsetHeight;
var sh = document.getElementById('storeheight').offsetHeight;
var sbh = document.getElementById('store-bot__overlay').offsetHeight;

function getHeight() {
	document.getElementById('last-news__overlay').style.height= h + "px";
}

function getDiscHeight() {
	document.getElementById('discussion__overlay').style.height= dh + "px";
}

function getStoreHeight() {
	document.getElementById('store__wrapper').style.height= sh + "px";
}

function getStoreOverlayHeight() {
	document.getElementById('store__overlay').style.maxHeight= (sh - sbh) + "px";
}

getHeight();
getDiscHeight();
getStoreHeight();
getStoreOverlayHeight();


window.addEventListener('resize', ()=> getHeight());
window.addEventListener('resize', ()=> getDiscHeight());
window.addEventListener('resize', ()=> getStoreHeight());
window.addEventListener('resize', ()=> getStoreOverlayHeight());
